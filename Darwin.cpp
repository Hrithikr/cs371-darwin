#include "Darwin.hpp"

using namespace std;

Darwin::Darwin(int rows, int columns)
{
    cout << "*** Darwin " << rows << "x" << columns << " ***" << endl;
    board = vector<vector<Creature*>>(rows, vector<Creature*>(columns, nullptr));
}

void Darwin::setupBoard()
{
    int row;
    int column;
    for (int x = 0; x < activeCreatures.size(); x++)
    {
        tie(row, column) = posOfCreatures[x];
        board[row][column] = &activeCreatures[x];
    }
    output(o);
    
    if(steps <= rounds)
    {
        cout << endl;
    }
}

bool Darwin::wall(int row, int column, char direction)
{
    if (row == 0 && direction == NORTH)
    {
        return true;
    }
    else if(row == board.size() - 1 && direction == SOUTH)
    {
        return true;
    }
    else if (column == board[0].size() && direction == EAST)
    {
        return true;
    }
    else if ( column == 0 && direction == WEST)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void Darwin::placeCreature(int row, int column, Creature temp)
{
    activeCreatures.push_back(temp);
    posOfCreatures.push_back(make_pair(row, column));

}
bool checkFront(int row, int column, Creature* temp, char direction)
{
    if (temp != nullptr)
    {
        return true;
    }
    else if (row == 0 && direction == NORTH)
    {
        return true;
    }
    else if (row == board[0].size() - 1 && direction == SOUTH)
    {
        return true;
    }
    else if(column == board[0].size() - 1 && direction == EAST)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void Darwin::moveCreature(int row, int column, char direction)
{
    Creature* temp = creatureInFront(row, column, direction);
    if (checkFront(row, column, temp, direction))
    { 
        return;
    }
    else
    {
        if (direction == NORTH)
        {
            board[row - 1][column] = board[row][column];
        }
        else if (direction == EAST)
        {
            board[row][column + 1] = board[row][column];
        }
        else if (direction == SOUTH)
        {
            board[row + 1][column] = board[row][column];
        }
        else if (direction == WEST)
        {
            board[row][column - 1] = board[row][column];
        }
        board[row][column] = nullptr;
    }

}

Creature* Darwin::creatureInFront(int row, int column, char direction)
{
    if (wall(row, column, direction))
    {
        return nullptr;
    }
    else
    {
        if (direction == NORTH)
        {
            return board[row - 1][column];
        }
        else if (direction == EAST)
        {
            return board[row][column + 1];
        }
        else if (direction == SOUTH)
        {
            return board[row + 1][column];
        }
        else if(direction == WEST)
        {
            return board[row][column - 1];
        }
    }
}

void Darwin::parameters(int round, int step)
{
    rounds = round;
    steps = step;
}

void Darwin::runGame()
{
    if(steps > rounds)
    {
        return;
    }
    for (int x  = 0; x < activeCreatures.size(); x++)
    {
        activeCreatures[x].setTurnOver(false);
    }

    for(int i = 0; i < board.size(); i++)
    {
        for(int j = 0; j < board[i].size(); j++)
        {
            Creature* curCreature = board[i][j];

            if (curCreature != nullptr && !curCreature->turnOver())
            {
                char direction = curCreature->getDirection();
                Creature* temp = creatureInFront(i,j, direction);
                bool check = wall(i,j,direction);
                Instructions action = board[i][j]->execute(temp, check);
                if (action == HOP)
                {
                    moveCreature(i,j, direction);
                }
                curCreature->setTurnOver(true);
            }
        }
    }
}

