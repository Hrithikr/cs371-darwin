#ifndef Species_h
#define Species_h

#include <tuple>
#include <iostream>	
#include <string>
#include <sstream>
#include <vector>
#include <cstdlib>
#include "Species.hpp"

enum Instructions {HOP, LEFT, RIGHT, INFECT, IF_EMPTY, IF_WALL, IF_RANDOM, IF_ENEMY, GO};
enum TypeOfSpecies {FOOD = 'f', HOPPER = 'h', ROVER = 'r', TRAP = 't'};

using namespace std;

class Species{
    private:
        vector<pair<Instructions, int>> list;
        char speciesType;
    public:
        Species(char temp);
        void Command(Instructions command, int index);
        pair<Instructions, int> runCommand(Species* temp, int turn, bool wall);
};
#endif      
