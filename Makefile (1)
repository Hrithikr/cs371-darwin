SHELL := /bin/bash
.DEFAULT_GOAL := all

ASTYLE        := astyle
CHECKTESTDATA := checktestdata
CPPCHECK      := cppcheck
DOXYGEN       := doxygen

ifeq ($(shell uname -s), Darwin)
    BOOST    := /usr/local/include/boost
    CXX      := clang++
    CXXFLAGS := --coverage -g -std=c++17 -Wall -Wextra -Wpedantic
    GCOV     := llvm-cov gcov
    GTEST    := /usr/local/src/googletest-master
    LDFLAGS  := -lgtest -lgtest_main
    LIB      := /usr/local/lib
    VALGRIND :=
else ifeq ($(shell uname -p), unknown)
    BOOST    := /usr/include/boost
    CXX      := g++
    CXXFLAGS := --coverage -g -std=c++17 -Wall -Wextra -Wpedantic
    GCOV     := gcov
    GTEST    := /usr/src/gtest
    LDFLAGS  := -lgtest -lgtest_main -pthread
    LIB      := /usr/lib
    VALGRIND := valgrind
else
    BOOST    := /usr/local/opt/boost-1.67/include/boost
    CXX      := g++-11
    CXXFLAGS := --coverage -g -std=c++17 -Wall -Wextra -Wpedantic
    GCOV     := gcov-11
    GTEST    := /usr/local/include/gtest
    LDFLAGS  := -L/usr/local/opt/boost-1.77/lib/ -lgtest -lgtest_main -pthread
    LIB      := /usr/local/lib
    VALGRIND := valgrind-3.17
endif

# run docker
docker:
	docker run --rm -i -t -v $(PWD):/usr/gcc -w /usr/gcc gpdowning/gcc

# get git config
config:
	git config -l

# get git log
Voting.log.txt:
	git log > Voting.log.txt

# get git status
status:
	make clean
	@echo
	git branch
	git remote -v
	git status

# download files from the Voting code repo
pull:
	make clean
	@echo
	git pull
	git status

# upload files to the Voting code repo
push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add Voting.cpp
	git add Voting.hpp
	-git add Voting.log.txt
	-git add html
	git add Makefile
	git add README.md
	git add RunVoting.cpp
	git add RunVoting.ctd.txt
	git add TestVoting.cpp
	git commit -m "another commit"
	git push
	git status

# compile run harness
RunVoting: Voting.hpp Voting.cpp RunVoting.cpp
	-$(CPPCHECK) Voting.cpp
	-$(CPPCHECK) RunVoting.cpp
	$(CXX) $(CXXFLAGS) Voting.cpp RunVoting.cpp -o RunVoting

# compile test harness
TestDarwin: Voting.hpp Voting.cpp TestDarwin.cpp
	-$(CPPCHECK) Darwin.cpp
	-$(CPPCHECK) TestDarwin.cpp
	$(CXX) $(CXXFLAGS) Voting.cpp TestDarwin.cpp -o TestDarwin $(LDFLAGS)

# run/test files, compile with make all
FILES :=        \
    RunDarwin  \
    TestDarwin

# compile all
all: $(FILES)

# execute test harness
test: TestDarwin
	-$(VALGRIND) ./TestDarwin

# execute test harness coverage
coverage: TestDarwin
ifeq ($(shell uname -s), Darwin)
	$(GCOV) TestDarwin.cpp | grep -B 2 "cpp.gcov"
else ifeq ($(shell uname -p), unknown)
	$(GCOV) TestDarwin-TestDarwin.cpp | grep -B 2 "cpp.gcov"
else
	$(GCOV) TestDarwin-TestDarwin.cpp | grep -B 2 "cpp.gcov"
endif

# clone the Voting test repo
../cs371p-darwin-tests:
	git clone git@gitlab.com:gpdowning/cs371p-darwin-tests.git

# test files in the Voting test repo
T_FILES := `ls ../cs371p-voting-tests/*.in.txt`

# check the integrity of all the test files in the Voting test repo
ctd-check: ../cs371p-darwin-tests
	-for v in $(T_FILES); do echo $(CHECKTESTDATA) RunDarwin.ctd.txt $$v; $(CHECKTESTDATA) RunDarwin.ctd.txt $$v; done

# generate a random input file
ctd-generate:
	for v in {1..100}; do $(CHECKTESTDATA) -g RunDarwin.ctd.txt >> RunVoting.gen.txt; done

# execute the run harness against a test file in the Voting test repo and diff with the expected output
../cs371p-darwin-tests/%: RunVoting
	$(CHECKTESTDATA) RunVoting.ctd.txt $@.in.txt
	./RunDarwin < $@.in.txt > RunVoting.tmp.txt
	-diff RunDarwin.tmp.txt $@.out.txt

# execute the run harness against your test files in the Voting test repo and diff with the expected output
run: ../cs371p-voting-tests
	##-make ../cs371p-darwin-tests/wchang8-RunVoting

# execute the run harness against all of the test files in the Voting test repo and diff with the expected output
run-all: ../cs371p-darwin-tests
	-for v in $(T_FILES); do make $${v/.in.txt/}; done

//CHANGE
# auto format the code
format:
	$(ASTYLE) Voting.cpp
	$(ASTYLE) Voting.hpp
	$(ASTYLE) RunVoting.cpp
	$(ASTYLE) TestVoting.cpp

# you must edit Doxyfile and
# set EXTRACT_ALL     to YES
# set EXTRACT_PRIVATE to YES
# set EXTRACT_STATIC  to YES
# create Doxfile
Doxyfile:
	$(DOXYGEN) -g

# create html directory
html: Doxyfile
	$(DOXYGEN) Doxyfile

# check files, check their existence with make check
C_FILES :=          \
    .gitignore      \
    .gitlab-ci.yml  \
    Voting.log.txt \
    html

# check the existence of check files
check: $(C_FILES)

# remove executables and temporary files
clean:
	rm -f  *.gcda
	rm -f  *.gcno
	rm -f  *.gcov
	rm -f  *.gen.txt
	rm -f  *.tmp.txt
	rm -f  RunVoting
	rm -f  TestVoting
	rm -rf *.dSYM

# remove executables, temporary files, and generated files
scrub:
	make clean
	rm -f  Voting.log.txt
	rm -f  Doxyfile
	rm -rf html
	rm -rf latex

# output versions of all tools
versions:
	@echo  'shell uname -p'
	@echo $(shell uname -p)

	@echo
	@echo  'shell uname -s'
	@echo $(shell uname -s)

	@echo
	@echo "% which $(ASTYLE)"
	@which $(ASTYLE)
	@echo
	@echo "% $(ASTYLE) --version"
	@$(ASTYLE) --version

	@echo
	@echo "% which $(CHECKTESTDATA)"
	@which $(CHECKTESTDATA)
	@echo
	@echo "% $(CHECKTESTDATA) --version"
	@$(CHECKTESTDATA) --version

	@echo
	@echo "% which cmake"
	@which cmake
	@echo
	@echo "% cmake --version"
	@cmake --version

	@echo
	@echo "% which $(CPPCHECK)"
	@which $(CPPCHECK)
	@echo
	@echo "% $(CPPCHECK) --version"
	@$(CPPCHECK) --version

	@echo
	@echo "% which $(DOXYGEN)"
	@which $(DOXYGEN)
	@echo
	@echo "% $(DOXYGEN) --version"
	@$(DOXYGEN) --version

	@echo
	@echo "% which $(CXX)"
	@which $(CXX)
	@echo
	@echo "% $(CXX) --version"
	@$(CXX) --version

	@echo "% which $(GCOV)"
	@which $(GCOV)
	@echo
	@echo "% $(GCOV) --version"
	@$(GCOV) --version

	@echo "% which git"
	@which git
	@echo
	@echo "% git --version"
	@git --version

	@echo
	@echo "% which make"
	@which make
	@echo
	@echo "% make --version"
	@make --version

ifneq ($(shell uname -s), Darwin)
	@echo
	@echo "% which $(VALGRIND)"
	@which $(VALGRIND)
	@echo
	@echo "% $(VALGRIND) --version"
	@$(VALGRIND) --version
endif

	@echo
	@echo "% which vim"
	@which vim
	@echo
	@echo "% vim --version"
	@vim --version

	@echo
	@echo "% grep \"#define BOOST_LIB_VERSION \" $(BOOST)/version.hpp"
	@grep "#define BOOST_LIB_VERSION " $(BOOST)/version.hpp

	@echo
	@echo "pkg-config --modversion gtest"
	@pkg-config --modversion gtest
	@echo
	@echo "% ls -al $(LIB)/libgtest*.a"
	@ls -al $(LIB)/libgtest*.a
