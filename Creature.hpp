#ifndef Creature_h
#define Creature_h

#include <tuple>
#include <iostream>	
#include <string>
#include <sstream>	
	
#include "Species.hpp"

using namespace std;

enum Directions {NORTH = 'n', EAST = 'e', SOUTH = 's', WEST = 'w'};

class Creature{
    private:
        Species* species;
        char direction;
        int counter;
        bool turnOver;
    public:
        Creature(Species* sp, char dir);
        void turnRight();
        void turnLeft();
        char getDirection();
        Species* getSpecieisType();
        bool turnOver();
        void setTurnOver();
        void infected(Species* infector);
        Intruction execute(Creature* curCreature, bool wall);

        

};

#endif