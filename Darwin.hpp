#ifndef Darwin_h
#define Darwin_h

#include <tuple>
#include <iostream>	
#include <string>
#include <sstream>
#include <vector>	
	
#include "Creature.hpp"

using namespace std;

class Darwin {
    private:
        vector<vector<Creature*>> board;
        vector<pair<int,int>> posOfCreatures;
        vector<Creature> activeCreatures;
        int rounds;
        int steps;
    public: 
        Darwin(int rows, int columns);
        void setupBoard();
        bool wall(int row, int column, char direction);
        void placeCreature(int row, int column, Creature temp);
        bool checkFront(int row, int column, Creature* temp, char direction);
        void moveCreature(int row, int column, char direction);
        Creature* creatureInFront(int row, int column, char direction);
        void Parameters(int rounds, int step);
        void runGame();
        void output();
        
};
#endif

