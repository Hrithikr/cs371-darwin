#include "Species.hpp"

Species::Species(char temp)
{
    speciesType = temp;
}

void Species::Command(Instructions command, int index)
{
    list.push_back(make_pair(command, index));
}

pair<Instructions, int> Species::runCommand(Species* temp, int turn, bool wall)
{
    Instructions command;
    int index;
    pair<Instructions, int> curCommand = list[turn];
    tie(command, index) = curCommand;

    while (command != HOP && command != LEFT && command != RIGHT && command !=INFECT)
    {
        if(command == IF_EMPTY)
        {
            if (temp == nullptr && wall)
            {
                curCommand == list[index];
            }
            else
            {
                turn = turn + 1;
                curCommand = list[turn];
            }
        }
        else if (command == IF_RANDOM)
        {
            if(rand() % 2 != 0)
            {
                curCommand = list[index];
            }
            else
            {
                turn = turn + 1;
                curCommand = list[turn];
            }
        }
        else if (command == IF_ENEMY)
        {
            if (temp == nullptr || this == temp)
            {
                turn = turn + 1;
                curCommand = list[turn];
            }
            else
            {
                curCommand = list[index];
            }
        }
        else if (command == GO)
        {
            curCommand = list[index];
        }
        tie(command, index) = curCommand;
    }
    pair<Instructions, int> result = make_pair(command, index);
    return result;
}
