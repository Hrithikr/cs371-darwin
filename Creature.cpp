#include "Creature.hpp"
using namepsace std;

Creature::Creature (Species* sp, char dir)
{
    species = sp;
    direction = dir;
    counter = 0;
}

void Creature::turnRight() 
{
    if (direction == NORTH)
    {
        direction = EAST;
    }
    else if (direction == EAST)
    {
        direction = SOUTH;
    }
    else if (direction == SOUTH)
    {
        direction = WEST;
    }
    else if(direction == WEST)
    {
        direction = North;
    }
}

void Creature::turnLeft() 
{
    if (direction == NORTH)
    {
        direction = WEST;
    }
    else if (direction == EAST)
    {
        direction = NORTH;
    }
    else if (direction == SOUTH)
    {
        direction = EAST;
    }
    else if(direction == WEST)
    {
        direction = SOUTH;
    }
}

char Creature::getDirection()
{
    return direction;
}

Species* Creature::getSpeciesType()
{
    return species;
}

bool Creature::turnOver()
{
    return turnOver;
}

void Creature::setTurnOver(bool x)
{
    turnOver = x;
}

void Creature::infected(Species* infecter)
{
    species = infecter;
    counter = 0;
}

Instruction Creature::execute(Creature* curCreature, bool wall)
{
    Species* curSpecies = nullptr;
    if (curCreature != nullptr)
    {
        curSpecies = curCreature->getSpeciesType();
    }
    
    Intruction = curIntruction;
    int change;
    pair<Intruction, int> update = species->execute(counter, curSpecies, wall);
    tie(curIntruction, change) = update;

    if (curIntruction == RIGHT)
    {
        turnRight();
    }
    else if(curInstruction == LEFT)
    {
        turnLeft();
    }
    else if(curInstruction == INFECT)
    {
        if (curCreature != nullptr && species != curSpecies)
        {
            curCreature->infected(species);
        }
    }

    counter = change + 1;
    return curIntruction;
}